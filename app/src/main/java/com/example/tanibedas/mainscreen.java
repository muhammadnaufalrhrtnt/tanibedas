package com.example.tanibedas;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class mainscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainscreen);
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.cd1);

        Button btn = findViewById(R.id.buttontest);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mainscreen.this,"Closed", Toast.LENGTH_SHORT).show();
                dialog.show();
            }
        });
    }
}
