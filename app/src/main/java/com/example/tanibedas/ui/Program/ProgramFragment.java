package com.example.tanibedas.ui.Program;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.tanibedas.databinding.FragmentProgramBinding;

public class ProgramFragment extends Fragment {

    private FragmentProgramBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ProgramViewModel programViewModel =
                new ViewModelProvider(this).get(ProgramViewModel.class);

        binding = FragmentProgramBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textProgram;
        programViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}