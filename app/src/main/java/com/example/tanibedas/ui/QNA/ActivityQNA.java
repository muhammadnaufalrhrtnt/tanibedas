package com.example.tanibedas.ui.QNA;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.tanibedas.R;

import java.util.ArrayList;

public class ActivityQNA extends AppCompatActivity {

    ArrayList<ModelClass> arrayList;
    RecyclerView recyclerView;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qna);
        recyclerView=findViewById(R.id.recycler_view);
        arrayList=new ArrayList<>();


        arrayList.add(new ModelClass("Bagaimana cara mendaftar sebagai petani di Tani Bedas?","aaa", false));
        arrayList.add(new ModelClass("Saya ingin mengubah nomor Handphone","Tekan tombol menu dikanan atas lalu masuk ke pengaturan akun nanti terdapat ubah data pribadi disana kita bisa mengganti nomor Handphone", false));
        arrayList.add(new ModelClass("Saya tidak dapat menghubungi PIC","aaa", false));
        arrayList.add(new ModelClass("Bagaimana cara mendapatkan hasil yang bagus?","aaa", false));
        arrayList.add(new ModelClass("Saya ingin mengganti password","aaa", false));

        adapter=new Adapter(arrayList,ActivityQNA.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}