package com.example.tanibedas.ui.MarketPlace;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MarketPlaceViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public MarketPlaceViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is MarketPlace fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}