package com.example.tanibedas.ui.Dashboard;

public class DashboarTanamanModel {
    private int timage;
    private String tjudul;

    public DashboarTanamanModel(int timage, String tjudul){
        this.timage = timage;
        this.tjudul = tjudul;
    }

    public int getImage() {
        return timage;
    }

    public String getJudul() {
        return tjudul;
    }
}
