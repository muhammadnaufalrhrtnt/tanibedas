package com.example.tanibedas.ui.SyaratKetentuan;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tanibedas.R;
import com.example.tanibedas.databinding.ActivityScrollingBinding;
import com.example.tanibedas.ui.BottomNavigation.BottomNavigation;

public class SyaratKetentuan extends AppCompatActivity {
    private ActivityScrollingBinding binding;

    Button btndf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        btndf = findViewById(R.id.btndf);

        btndf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent(SyaratKetentuan.this, BottomNavigation.class);
                startActivity(i);
                finish();

            }
        });
    }
}
