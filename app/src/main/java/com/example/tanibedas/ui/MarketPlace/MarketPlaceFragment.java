package com.example.tanibedas.ui.MarketPlace;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tanibedas.databinding.FragmentMarketPlaceBinding;


public class MarketPlaceFragment extends Fragment {

    private FragmentMarketPlaceBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        MarketPlaceViewModel marketPlaceViewModel =
                new ViewModelProvider(this).get(MarketPlaceViewModel.class);

        binding = FragmentMarketPlaceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textMarketPlace;
        marketPlaceViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}