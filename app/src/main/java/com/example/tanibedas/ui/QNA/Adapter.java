package com.example.tanibedas.ui.QNA;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tanibedas.R;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    ArrayList<ModelClass> arrayList;
    Context context;

    public Adapter(ArrayList<ModelClass> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.qna_design,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {

        holder.pertanyaan.setText(arrayList.get(position).title);
        holder.desc.setText(arrayList.get(position).description);


        holder.pertanyaan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(arrayList.get(position).isVisible)
                {
                    holder.desc.setVisibility(View.GONE);
                    holder.desc_line.setVisibility(View.GONE);
                    holder.pertanyaan_line.setVisibility(View.VISIBLE);
                    arrayList.get(position).isVisible=false;
                }
                else
                {
                    holder.desc.setVisibility(View.VISIBLE);
                    holder.desc_line.setVisibility(View.VISIBLE);
                    holder.pertanyaan_line.setVisibility(View.GONE);
                    arrayList.get(position).isVisible=true;
                }
            }

        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView pertanyaan;
        TextView desc;
        RelativeLayout pertanyaan_line;
        RelativeLayout desc_line;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pertanyaan=itemView.findViewById(R.id.pertanyaan);
            desc=itemView.findViewById(R.id.desc);
            desc_line=itemView.findViewById(R.id.desc_line);
            pertanyaan_line=itemView.findViewById(R.id.pertanyaan_line);

        }
    }
}
