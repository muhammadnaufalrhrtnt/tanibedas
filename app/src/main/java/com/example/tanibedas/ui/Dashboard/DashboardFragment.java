package com.example.tanibedas.ui.Dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.tanibedas.R;
import com.example.tanibedas.adapter.DashboardAdapter.DashboarTanamanAdapter;
import com.example.tanibedas.adapter.DashboardAdapter.DashboardPanelAdapter;
import com.example.tanibedas.databinding.FragmentDashboardBinding;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

//      carousel
        ImageSlider imgs = binding.ImageSlider;

        ArrayList<SlideModel> slideModels = new ArrayList<>();

        slideModels.add(new SlideModel("https://cdn.pixabay.com/photo/2016/03/08/20/03/flag-1244649__340.jpg", ScaleTypes.FIT));
        slideModels.add(new SlideModel("https://cdn.pixabay.com/photo/2016/03/08/20/03/flag-1244648__340.jpg", ScaleTypes.FIT));
        slideModels.add(new SlideModel("https://cdn.pixabay.com/photo/2018/04/26/12/14/travel-3351825__340.jpg", ScaleTypes.FIT));
        slideModels.add(new SlideModel("https://cdn.pixabay.com/photo/2013/03/01/18/40/crispus-87928__340.jpg", ScaleTypes.FIT));

        imgs.setImageList(slideModels, ScaleTypes.FIT);




//        panel
        RecyclerView recpanel = binding.lypanel;
        recpanel.setHasFixedSize(true);
        recpanel.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        List<DashboardPanelModel> panelModelList = new ArrayList<>();

        panelModelList.add(new DashboardPanelModel(R.drawable.termo, "Hitung Suhu"));
        panelModelList.add(new DashboardPanelModel(R.drawable.termo, "Hitung Luas Tanah"));
        panelModelList.add(new DashboardPanelModel(R.drawable.termo, "Kalender Penanaman"));
        panelModelList.add(new DashboardPanelModel(R.drawable.termo, "Kalkulasi Pupuk & Usaha"));

        DashboardPanelAdapter panelAdapter = new DashboardPanelAdapter(panelModelList);

        recpanel.setAdapter(panelAdapter);

//        tanaman
        RecyclerView rectanaman = binding.lytanaman;
        rectanaman.setHasFixedSize(true);
        rectanaman.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        List<DashboarTanamanModel> tanamanModelList = new ArrayList<>();

        tanamanModelList.add(new DashboarTanamanModel(R.drawable.jagung, "jagung"));
        tanamanModelList.add(new DashboarTanamanModel(R.drawable.jagung, "jagung"));
        tanamanModelList.add(new DashboarTanamanModel(R.drawable.jagung, "jagung"));
        tanamanModelList.add(new DashboarTanamanModel(R.drawable.jagung, "jagung"));
        tanamanModelList.add(new DashboarTanamanModel(R.drawable.jagung, "jagung"));

        DashboarTanamanAdapter tanamanAdapter = new DashboarTanamanAdapter(tanamanModelList);

        rectanaman.setAdapter(tanamanAdapter);

//        rekomendasi
        RecyclerView recRecomend = binding.lyrekomen;
        recRecomend.setHasFixedSize(true);
        recRecomend.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        final TextView textView = binding.textDashboard;
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
