package com.example.tanibedas.ui.QNA;

public class ModelClass {
    String title;
    String description;
    boolean isVisible;

    public ModelClass(String title, String description, boolean isVisible) {
        this.title = title;
        this.description = description;
        this.isVisible = isVisible;
    }
}
