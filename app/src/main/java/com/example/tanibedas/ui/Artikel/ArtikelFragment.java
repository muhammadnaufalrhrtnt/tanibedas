package com.example.tanibedas.ui.Artikel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tanibedas.databinding.FragmentArtikelBinding;


public class ArtikelFragment extends Fragment {

    private FragmentArtikelBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ArtikelViewModel dashboardViewModel =
                new ViewModelProvider(this).get(ArtikelViewModel.class);

        binding = FragmentArtikelBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textArtikel;
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}