package com.example.tanibedas.ui.Dashboard;

public class DashboardPanelModel {
    private int pimage;
    private String pjudul;

    public DashboardPanelModel(int pimage, String pjudul){
        this.pimage = pimage;
        this.pjudul = pjudul;
    }

    public int getImage() {
        return pimage;
    }

    public String getJudul() {
        return pjudul;
    }
}
