package com.example.tanibedas.adapter.DashboardAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tanibedas.R;
import com.example.tanibedas.ui.Dashboard.DashboardPanelModel;

import java.util.List;

public class DashboardPanelAdapter extends RecyclerView.Adapter<DashboardPanelAdapter.panelViewHolder> {

    private List<DashboardPanelModel> panelModelList;

    public DashboardPanelAdapter(List<DashboardPanelModel> panelModelList){
        this.panelModelList = panelModelList;
    }
    @NonNull
    @Override
    public panelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dasboard_panel, parent, false);
        return new panelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull panelViewHolder holder, int position) {
        holder.judul.setText(panelModelList.get(position).getJudul());
        holder.image.setImageResource(panelModelList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return panelModelList.size();
    }

    public class panelViewHolder extends RecyclerView.ViewHolder{
        private TextView judul;
        private ImageView image;
        public panelViewHolder(@NonNull View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.tpanel);
            image = itemView.findViewById(R.id.ipanel);
        }
    }
}
