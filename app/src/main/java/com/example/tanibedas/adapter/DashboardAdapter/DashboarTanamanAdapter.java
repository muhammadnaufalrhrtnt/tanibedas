package com.example.tanibedas.adapter.DashboardAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tanibedas.R;
import com.example.tanibedas.ui.Dashboard.DashboarTanamanModel;

import java.util.List;

public class DashboarTanamanAdapter extends RecyclerView.Adapter<DashboarTanamanAdapter.TanamanViewHolder> {

    private List<DashboarTanamanModel> tanamanModelList;
    public  DashboarTanamanAdapter(List<DashboarTanamanModel> tanamanModelList){
        this.tanamanModelList = tanamanModelList;
    }
    @NonNull
    @Override
    public TanamanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboar_tanaman, parent, false);
        return new TanamanViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TanamanViewHolder holder, int position) {
        holder.tjudul.setText(tanamanModelList.get(position).getJudul());
        holder.timage.setImageResource(tanamanModelList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return tanamanModelList.size();
    }

    public class TanamanViewHolder extends RecyclerView.ViewHolder{


        private ImageView timage;
        private TextView tjudul;
        public TanamanViewHolder(@NonNull View itemView) {
            super(itemView);

            timage =itemView.findViewById(R.id.itanaman);
            tjudul =itemView.findViewById(R.id.ttanaman);
        }
    }
}
